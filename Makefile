EVINCE=/usr/bin/evince
LATEX=/usr/bin/latex
PDFLATEX=/usr/bin/latex
DVIPDF=/usr/bin/dvipdf
DETEX=/usr/bin/detex
BIBTEX=/usr/bin/bibtex
SED=/usr/bin/sed
SCP=/usr/bin/scp
QRENCODE=/usr/bin/qrencode

ifndef $(OUT)
OUT=$(DOC)
endif

ifndef $(NETWORK)
NETWORK=example
PASS=12345678!
endif

ifndef $(DEF)
DEF=\def\pass{$(PASS)}\def\network{$(NETWORK)}
endif

all:
	cat template.tex | $(SED) -e 's/#NETWORK#/$(NETWORK)/' \
		             | $(SED) -e 's/#PASSWORD#/$(PASS)/' > wificode.tex
	make DOC=wificode build

clean:
	rm -f wificode*

build:
	$(QRENCODE) -o $(OUT).png -s 6 -l H 'WIFI:T:WPA2;S:$(NETWORK);P:$(PASS);;'
	$(PDFLATEX) -jobname $(OUT) -output-format=pdf "$(DEF)\input $(DOC)"
	@$(EVINCE) $(OUT).pdf
