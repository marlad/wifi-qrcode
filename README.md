# Wificode

Automatically create a pdf document with the wifi code and include a QR code.

# Setup LaTeX

`sudo apt install -y qrencode texlive texlive-pictures texlive-latex-extra`

# Create pdf file

`make NETWORK=example PASS=12345678!`

# Example output

![Example](/example.png?raw=true)
